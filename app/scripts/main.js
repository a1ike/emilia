$(document).ready(function () {
  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false,
  });

  $('.e-home-seo__content_hide').on('click', function (e) {
    e.preventDefault();

    $(this).toggleClass('e-home-seo__content_opened');
  });

  $('.show-step_1').on('click', function (e) {
    e.preventDefault();

    $('.e-order-step').removeClass('e-order-step_active');
    $('.e-order-step_1').addClass('e-order-step_active');

    $('.e-order__point').removeClass('e-order__point_active');
    $('.e-order__point').removeClass('e-order__point_done');
    $('.e-order__point_1').addClass('e-order__point_active');
  });

  $('.show-step_2').on('click', function (e) {
    e.preventDefault();

    $('.e-order-step').removeClass('e-order-step_active');
    $('.e-order-step_2').addClass('e-order-step_active');

    $('.e-order__point').removeClass('e-order__point_active');
    $('.e-order__point').removeClass('e-order__point_done');
    $('.e-order__point_1').addClass('e-order__point_done');
    $('.e-order__point_2').addClass('e-order__point_active');
  });

  $('.show-step_3').on('click', function (e) {
    e.preventDefault();

    $('.e-order-step').removeClass('e-order-step_active');
    $('.e-order-step_3').addClass('e-order-step_active');

    $('.e-order__point').removeClass('e-order__point_active');
    $('.e-order__point').removeClass('e-order__point_done');
    $('.e-order__point_1').addClass('e-order__point_done');
    $('.e-order__point_2').addClass('e-order__point_done');
    $('.e-order__point_3').addClass('e-order__point_active');
  });

  $('.show-step_4').on('click', function (e) {
    e.preventDefault();

    $('.e-order-step').removeClass('e-order-step_active');
    $('.e-order-step_4').addClass('e-order-step_active');
  });

  $(window).on('load resize', function (e) {
    if ($(window).width() < 1199) {
      $('.e-header__person').detach().appendTo('.e-header__mob');

      $('.e-footer__subtitle').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('e-footer__subtitle_active');
        $(this).next().slideToggle('fast');
      });

      $('.e-header__navtitle').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('e-header__navtitle_active');
        $(this).next().slideToggle('fast');
      });

      $('.e-header__fav').detach().prependTo('.e-header__menu');

      $('.e-footer_4').detach().insertAfter('.e-footer__logo');
      $('.e-footer_3').detach().insertAfter('.e-footer__logo');
      $('.e-footer_2').detach().insertAfter('.e-footer__logo');
      $('.e-footer_1').detach().insertAfter('.e-footer__logo');

      $('.e-aside').detach().prependTo('.e-goods-mob-filter');
      $('.e-goods__sort').detach().prependTo('.e-goods-mob-sort');

      $('#ig_2').children().clone().appendTo('#ig_1');
      $('#ig_2').hide();

      $('.e-header__toggle').on('click', function (e) {
        e.preventDefault();
        $('.e-header__toggle').toggle();
        $('.e-header__toggle_close').toggle();
        $('.e-header__menu').toggle();
      });

      $('.e-header__toggle_close').on('click', function (e) {
        e.preventDefault();
        $('.e-header__toggle').toggle();
        $('.e-header__toggle_close').toggle();
        $('.e-header__menu').toggle();
      });

      $('.e-goods-mob__filter').on('click', function (e) {
        e.preventDefault();

        $('.e-goods-mob-sort').hide();
        $(this).next().slideToggle('fast');
      });

      $('.e-goods-mob__sort').on('click', function (e) {
        e.preventDefault();

        $('.e-goods-mob-filter').hide();
        $(this).next().slideToggle('fast');
      });

      new Swiper('.e-item__cards', {
        pagination: {
          el: '.e-item__cards .swiper-pagination',
        },
      });
      $('.e-home-ig-card').css({
        height: $('.e-home-ig-card').outerWidth() + 'px',
      });
      $('.e-home-ig__cards').css({
        'max-height': $('.e-home-ig-card').outerWidth() * 4 + 'px',
      });
    } else {
      $('.e-header__basket-icon').on('click', function (e) {
        e.preventDefault();
        $('.e-header-basket').toggle();
      });

      $('.e-header__toggle').on('click', function (e) {
        e.preventDefault();
        $('.e-header__menu').toggle();
      });

      $('.e-header__menu-close').on('click', function (e) {
        e.preventDefault();
        $('.e-header__menu').toggle();
      });

      $('.e-aside-collapse').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('e-aside-collapse_active');
        $(this).next().slideToggle('fast');
      });
    }
  });

  $('.e-item__collapse').on('click', function (e) {
    e.preventDefault();
    $(this).toggleClass('e-item__collapse_active');
    $(this).next().slideToggle('fast');
  });

  window.onscroll = function () {
    scrollFunction();
  };

  function scrollFunction() {
    if (
      document.body.scrollTop > 30 ||
      document.documentElement.scrollTop > 30
    ) {
      $('.e-header__news').hide('fast');
    } else {
      $('.e-header__news').show('fast');
    }
  }

  $('.e-header-basket__close').on('click', function (e) {
    e.preventDefault();
    $('.e-header-basket').toggle();
  });

  $('.e-header-basket-card__minus').on('click', function (e) {
    e.preventDefault();

    var currentVal = $(this).next().val();

    if (currentVal > 1) {
      $(this)
        .next()
        .val(--currentVal);
    }
  });

  $('.e-header-basket-card__plus').on('click', function (e) {
    e.preventDefault();

    var currentVal = $(this).prev().val();

    if (currentVal < 99) {
      $(this)
        .prev()
        .val(++currentVal);
    }
  });

  $('.e-basket-card__minus').on('click', function (e) {
    e.preventDefault();

    var currentVal = $(this).next().val();

    if (currentVal > 1) {
      $(this)
        .next()
        .val(--currentVal);
    }
  });

  $('.e-basket-card__plus').on('click', function (e) {
    e.preventDefault();

    var currentVal = $(this).prev().val();

    if (currentVal < 99) {
      $(this)
        .prev()
        .val(++currentVal);
    }
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();

    $('.e-modal').toggle();
  });

  $('.e-modal__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'e-modal__centered') {
      $('.e-modal').hide();
    }
  });

  $('.e-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.e-modal').hide();
  });

  $('.open-search').on('click', function (e) {
    e.preventDefault();

    $('.e-search').toggle();
  });

  $('.e-search__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'e-search__centered') {
      $('.e-search').hide();
    }
  });

  $('.e-search__close').on('click', function (e) {
    e.preventDefault();
    $('.e-search').hide();
  });

  $('.e-home-cards-card__like').on('click', function (e) {
    e.preventDefault();
    $(this).toggleClass('e-home-cards-card__like_liked');
  });

  $('.e-item__like').on('click', function (e) {
    e.preventDefault();
    $(this).toggleClass('e-item__like_liked');
  });

  new Swiper('.e-top', {
    speed: 1000,
    fadeEffect: {
      crossFade: true,
    },
    effect: 'fade',
    navigation: {
      nextEl: '.e-top .swiper-button-next',
      prevEl: '.e-top .swiper-button-prev',
    },
    pagination: {
      el: '.e-top .swiper-pagination',
    },
    loop: true,
  });

  new Swiper('#home-sale', {
    navigation: {
      nextEl: '#home-sale .swiper-button-next',
      prevEl: '#home-sale .swiper-button-prev',
    },
    slidesPerView: 'auto',
    spaceBetween: 20,
    centeredSlides: true,
    loop: true,
    breakpoints: {
      1200: {
        centeredSlides: false,
        spaceBetween: 30,
        slidesPerView: 4,
        loop: true,
      },
    },
  });

  new Swiper('#home-new', {
    navigation: {
      nextEl: '#home-new .swiper-button-next',
      prevEl: '#home-new .swiper-button-prev',
    },
    slidesPerView: 'auto',
    spaceBetween: 20,
    centeredSlides: true,
    loop: true,
    breakpoints: {
      1200: {
        centeredSlides: false,
        spaceBetween: 30,
        slidesPerView: 4,
        loop: true,
      },
    },
  });

  new SimpleBar($('.e-header-basket__cards')[0], {
    autoHide: false,
    scrollbarMinSize: 11,
    scrollbarMaxSize: 11,
  });
});
